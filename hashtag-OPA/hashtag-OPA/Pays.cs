﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hashtag_OPA
{
    class Pays
    {
        public int PIB;

        public int population_active;
        public double tauxChomage;

        public int importations;
        public int exportations;

        public int consommation;

        public int revenuMenages;
        public int revenuEntreprises;

        public int recettes;
        public int depenses;

        public int soldeBudgetaire;

        public int endettement;


        public Pays()
        {

        }

    }
}
