﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hashtag_OPA
{
    class G_Jeu
    {
        public G_Jeu()
        {

        }

        //renvoie un pays avec des valeurs initiales
        public static Pays jeuDessai()
        {
            Pays p = new Pays();
            p.PIB = 11426;
            p.population_active = 1058;
            p.tauxChomage = 10;
            p.importations = 4014;
            p.exportations = 4281;
            p.consommation = 6959;
            p.revenuMenages = 3398;
            p.revenuEntreprises = 7998;
            p.recettes = 1650;
            p.depenses = 1800;
            p.soldeBudgetaire = -150;
            p.endettement = 800;

            return p;
        }

        //calcule les nouvelles valeurs d'un pays pour des décisions
        public static void tourSuivant(Pays p, int[] decisions)
        {

            double impots = decisions[0];
            int depenses = decisions[1];
            int investissements = decisions[2];
            int aides = decisions[3];

            

            double i;

            if (impots <= 10)
            {
                i = -10 + impots * 5 / 10;
            }
            else if(impots <= 20)
            {
                i = -15 + impots;
            }
            else
            {
                i = -5 + impots * 5 / 10;
            }
            p.tauxChomage += i;

            
            switch (investissements)
            {
                case 0:
                    i = 15;
                    break;
                case 100:
                    i = 12.5;
                    break;
                case 200:
                    i = 10;
                    break;
                case 300:
                    i = 7.5;
                    break;
                case 400:
                    i = 5;
                    break;
                case 500:
                    i = 2.5;
                    break;
                case 600:
                    i = 0;
                    break;
                case 700:
                    i = -2.5;
                    break;
                case 800:
                    i = -5;
                    break;
                case 900:
                    i = -7.5;
                    break;
                case 1000:
                    i = -10;
                    break;
            }
            p.tauxChomage += i;

            if (p.tauxChomage < 0)
            {
                p.tauxChomage = 0;
            }

            double pop = p.population_active * 1.01;
            p.population_active = Convert.ToInt32(Math.Floor(pop));

            double PIB = (100 - p.tauxChomage) / 100 * p.population_active * 12;
            p.PIB = Convert.ToInt32(Math.Floor(PIB));

            double revenuMenages = 0.3 * p.PIB;
            p.revenuMenages = Convert.ToInt32(Math.Floor(revenuMenages));

            double revenuEntreprises = 0.7 * p.PIB;
            p.revenuEntreprises = Convert.ToInt32(Math.Floor(revenuEntreprises));

            double recettes = p.PIB * impots / 100;
            p.recettes = Convert.ToInt32(Math.Floor(recettes));

            double DP = investissements + aides + depenses;
            p.depenses = Convert.ToInt32(Math.Floor(DP));

            double solde = p.recettes - p.depenses;
            p.soldeBudgetaire = Convert.ToInt32(Math.Floor(solde));

            p.endettement += p.soldeBudgetaire;


            double a = 0.2;
            double conso = a * p.PIB + investissements * 0.5 + aides * 0.5 + p.depenses * 0.5 + p.population_active * 3;
            p.consommation = Convert.ToInt32(Math.Floor(conso));

            double c = 0.2;
            double import = c * p.PIB + investissements * 2 + p.population_active * 0.5;
            p.importations = Convert.ToInt32(Math.Floor(import));

            double export = p.PIB + p.importations - p.consommation - investissements*7;
            p.exportations = Convert.ToInt32(Math.Floor(export));
        }

        //renvoie les valeurs du pays donné en paramètre
        public static double[] valeursPays(Pays p)
        {
            double[] valeursPays = new double[12];
            valeursPays[0] = p.PIB;
            valeursPays[1] = p.population_active;
            valeursPays[2] = p.tauxChomage;
            valeursPays[3] = p.importations;
            valeursPays[4] = p.exportations;
            valeursPays[5] = p.consommation;
            valeursPays[6] = p.revenuMenages;
            valeursPays[7] = p.revenuEntreprises;
            valeursPays[8] = p.recettes;
            valeursPays[9] = p.depenses;
            valeursPays[10] = p.soldeBudgetaire;
            valeursPays[11] = p.endettement;

            return valeursPays;
        }
    }
}
