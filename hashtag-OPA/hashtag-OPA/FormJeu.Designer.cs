﻿namespace hashtag_OPA
{
    partial class FormJeu
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 12000D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 12000D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 12000D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 12000D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 2D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 2D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint7 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 2D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint8 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 2D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint9 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormJeu));
            this.tcJeu = new System.Windows.Forms.TabControl();
            this.tpResultats = new System.Windows.Forms.TabPage();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.panelRes3 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.panelRes2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonRecommencer = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panelRes0 = new System.Windows.Forms.Panel();
            this.labelEndet0 = new System.Windows.Forms.Label();
            this.labelSB0 = new System.Windows.Forms.Label();
            this.labelPIB0 = new System.Windows.Forms.Label();
            this.labelRE0 = new System.Windows.Forms.Label();
            this.labelDP0 = new System.Windows.Forms.Label();
            this.labelPop0 = new System.Windows.Forms.Label();
            this.labelTC0 = new System.Windows.Forms.Label();
            this.labelRP0 = new System.Windows.Forms.Label();
            this.labelImport0 = new System.Windows.Forms.Label();
            this.labelExport0 = new System.Windows.Forms.Label();
            this.labelConso0 = new System.Windows.Forms.Label();
            this.labelRM0 = new System.Windows.Forms.Label();
            this.labelObjectifs = new System.Windows.Forms.Label();
            this.panelRes1 = new System.Windows.Forms.Panel();
            this.labelPIB1 = new System.Windows.Forms.Label();
            this.labelEndet1 = new System.Windows.Forms.Label();
            this.labelPop1 = new System.Windows.Forms.Label();
            this.labelSB1 = new System.Windows.Forms.Label();
            this.labelDP1 = new System.Windows.Forms.Label();
            this.labelRP1 = new System.Windows.Forms.Label();
            this.labelRE1 = new System.Windows.Forms.Label();
            this.labelRM1 = new System.Windows.Forms.Label();
            this.labelConso1 = new System.Windows.Forms.Label();
            this.labelTC1 = new System.Windows.Forms.Label();
            this.labelImport1 = new System.Windows.Forms.Label();
            this.labelExport1 = new System.Windows.Forms.Label();
            this.labelDepensesPubliques = new System.Windows.Forms.Label();
            this.labelRevenusEntreprises = new System.Windows.Forms.Label();
            this.labelImport = new System.Windows.Forms.Label();
            this.labelRevenusMenages = new System.Windows.Forms.Label();
            this.labelPopActive = new System.Windows.Forms.Label();
            this.labelExport = new System.Windows.Forms.Label();
            this.labelRecettesPubliques = new System.Windows.Forms.Label();
            this.labelSoldeBudgetaire = new System.Windows.Forms.Label();
            this.labelEndettement = new System.Windows.Forms.Label();
            this.labelConso = new System.Windows.Forms.Label();
            this.labelTauxChomage = new System.Windows.Forms.Label();
            this.labelPIB = new System.Windows.Forms.Label();
            this.labelDesDP = new System.Windows.Forms.Label();
            this.labelDesInvestissements = new System.Windows.Forms.Label();
            this.labelDesAides = new System.Windows.Forms.Label();
            this.buttonAidesMoins = new System.Windows.Forms.Button();
            this.buttonAidesPlus = new System.Windows.Forms.Button();
            this.buttonInvMoins = new System.Windows.Forms.Button();
            this.buttonInvPlus = new System.Windows.Forms.Button();
            this.buttonDPMoins = new System.Windows.Forms.Button();
            this.buttonDPPlus = new System.Windows.Forms.Button();
            this.buttonImpotsMoins = new System.Windows.Forms.Button();
            this.buttonImpotsPlus = new System.Windows.Forms.Button();
            this.labelDesImpots = new System.Windows.Forms.Label();
            this.labelAides = new System.Windows.Forms.Label();
            this.labelInvestissements = new System.Windows.Forms.Label();
            this.labelDepenses = new System.Windows.Forms.Label();
            this.labelImpots = new System.Windows.Forms.Label();
            this.toolTipDecisions = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.bSuivant = new System.Windows.Forms.Button();
            this.chartPIB = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartTC = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartSB = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tcInstructions = new System.Windows.Forms.TabControl();
            this.Instructions = new System.Windows.Forms.TabPage();
            this.labelInstructions = new System.Windows.Forms.Label();
            this.tcJeu.SuspendLayout();
            this.tpResultats.SuspendLayout();
            this.panelRes3.SuspendLayout();
            this.panelRes2.SuspendLayout();
            this.panelRes0.SuspendLayout();
            this.panelRes1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPIB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartTC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartSB)).BeginInit();
            this.tcInstructions.SuspendLayout();
            this.Instructions.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcJeu
            // 
            this.tcJeu.Controls.Add(this.tpResultats);
            this.tcJeu.Location = new System.Drawing.Point(12, 12);
            this.tcJeu.Name = "tcJeu";
            this.tcJeu.SelectedIndex = 0;
            this.tcJeu.Size = new System.Drawing.Size(776, 426);
            this.tcJeu.TabIndex = 0;
            // 
            // tpResultats
            // 
            this.tpResultats.Controls.Add(this.label31);
            this.tpResultats.Controls.Add(this.label30);
            this.tpResultats.Controls.Add(this.label29);
            this.tpResultats.Controls.Add(this.panelRes3);
            this.tpResultats.Controls.Add(this.panelRes2);
            this.tpResultats.Controls.Add(this.buttonRecommencer);
            this.tpResultats.Controls.Add(this.label28);
            this.tpResultats.Controls.Add(this.label27);
            this.tpResultats.Controls.Add(this.label26);
            this.tpResultats.Controls.Add(this.label25);
            this.tpResultats.Controls.Add(this.panelRes0);
            this.tpResultats.Controls.Add(this.labelObjectifs);
            this.tpResultats.Controls.Add(this.panelRes1);
            this.tpResultats.Controls.Add(this.labelDepensesPubliques);
            this.tpResultats.Controls.Add(this.labelRevenusEntreprises);
            this.tpResultats.Controls.Add(this.labelImport);
            this.tpResultats.Controls.Add(this.labelRevenusMenages);
            this.tpResultats.Controls.Add(this.labelPopActive);
            this.tpResultats.Controls.Add(this.labelExport);
            this.tpResultats.Controls.Add(this.labelRecettesPubliques);
            this.tpResultats.Controls.Add(this.labelSoldeBudgetaire);
            this.tpResultats.Controls.Add(this.labelEndettement);
            this.tpResultats.Controls.Add(this.labelConso);
            this.tpResultats.Controls.Add(this.labelTauxChomage);
            this.tpResultats.Controls.Add(this.labelPIB);
            this.tpResultats.Location = new System.Drawing.Point(4, 22);
            this.tpResultats.Name = "tpResultats";
            this.tpResultats.Padding = new System.Windows.Forms.Padding(3);
            this.tpResultats.Size = new System.Drawing.Size(768, 400);
            this.tpResultats.TabIndex = 0;
            this.tpResultats.Text = "Résultats";
            this.tpResultats.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(633, 333);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 13);
            this.label31.TabIndex = 40;
            this.label31.Text = "Positif";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(633, 86);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 13);
            this.label30.TabIndex = 39;
            this.label30.Text = "2";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(633, 42);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(37, 13);
            this.label29.TabIndex = 38;
            this.label29.Text = "12000";
            // 
            // panelRes3
            // 
            this.panelRes3.Controls.Add(this.label13);
            this.panelRes3.Controls.Add(this.label14);
            this.panelRes3.Controls.Add(this.label15);
            this.panelRes3.Controls.Add(this.label16);
            this.panelRes3.Controls.Add(this.label17);
            this.panelRes3.Controls.Add(this.label18);
            this.panelRes3.Controls.Add(this.label19);
            this.panelRes3.Controls.Add(this.label20);
            this.panelRes3.Controls.Add(this.label21);
            this.panelRes3.Controls.Add(this.label22);
            this.panelRes3.Controls.Add(this.label23);
            this.panelRes3.Controls.Add(this.label24);
            this.panelRes3.Location = new System.Drawing.Point(501, 42);
            this.panelRes3.Name = "panelRes3";
            this.panelRes3.Size = new System.Drawing.Size(55, 342);
            this.panelRes3.TabIndex = 37;
            this.panelRes3.Tag = "1";
            this.panelRes3.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "label9";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "label24";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 44);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "label4";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 75);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "label18";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 97);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 13);
            this.label17.TabIndex = 18;
            this.label17.Text = "label19";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 129);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = "label20";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 167);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 20;
            this.label19.Text = "label21";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 192);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "label22";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 231);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 13);
            this.label21.TabIndex = 22;
            this.label21.Text = "label23";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 253);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 13);
            this.label22.TabIndex = 7;
            this.label22.Text = "label8";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(3, 291);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "label5";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(3, 321);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 15;
            this.label24.Text = "label16";
            // 
            // panelRes2
            // 
            this.panelRes2.Controls.Add(this.label1);
            this.panelRes2.Controls.Add(this.label2);
            this.panelRes2.Controls.Add(this.label3);
            this.panelRes2.Controls.Add(this.label4);
            this.panelRes2.Controls.Add(this.label5);
            this.panelRes2.Controls.Add(this.label6);
            this.panelRes2.Controls.Add(this.label7);
            this.panelRes2.Controls.Add(this.label8);
            this.panelRes2.Controls.Add(this.label9);
            this.panelRes2.Controls.Add(this.label10);
            this.panelRes2.Controls.Add(this.label11);
            this.panelRes2.Controls.Add(this.label12);
            this.panelRes2.Location = new System.Drawing.Point(395, 42);
            this.panelRes2.Name = "panelRes2";
            this.panelRes2.Size = new System.Drawing.Size(55, 342);
            this.panelRes2.TabIndex = 36;
            this.panelRes2.Tag = "1";
            this.panelRes2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "label9";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "label24";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "label4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "label18";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "label19";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "label20";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "label21";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 192);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "label22";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 231);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "label23";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 253);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "label8";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 291);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "label5";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 321);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "label16";
            // 
            // buttonRecommencer
            // 
            this.buttonRecommencer.Location = new System.Drawing.Point(678, 6);
            this.buttonRecommencer.Name = "buttonRecommencer";
            this.buttonRecommencer.Size = new System.Drawing.Size(84, 23);
            this.buttonRecommencer.TabIndex = 35;
            this.buttonRecommencer.Text = "Recommencer";
            this.buttonRecommencer.UseVisualStyleBackColor = true;
            this.buttonRecommencer.Visible = false;
            this.buttonRecommencer.Click += new System.EventHandler(this.buttonRecommencer_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(498, 18);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(21, 13);
            this.label28.TabIndex = 34;
            this.label28.Text = "R3";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(392, 18);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(21, 13);
            this.label27.TabIndex = 33;
            this.label27.Text = "R2";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(292, 18);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(21, 13);
            this.label26.TabIndex = 32;
            this.label26.Text = "R1";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(192, 17);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(21, 13);
            this.label25.TabIndex = 31;
            this.label25.Text = "R0";
            // 
            // panelRes0
            // 
            this.panelRes0.Controls.Add(this.labelEndet0);
            this.panelRes0.Controls.Add(this.labelSB0);
            this.panelRes0.Controls.Add(this.labelPIB0);
            this.panelRes0.Controls.Add(this.labelRE0);
            this.panelRes0.Controls.Add(this.labelDP0);
            this.panelRes0.Controls.Add(this.labelPop0);
            this.panelRes0.Controls.Add(this.labelTC0);
            this.panelRes0.Controls.Add(this.labelRP0);
            this.panelRes0.Controls.Add(this.labelImport0);
            this.panelRes0.Controls.Add(this.labelExport0);
            this.panelRes0.Controls.Add(this.labelConso0);
            this.panelRes0.Controls.Add(this.labelRM0);
            this.panelRes0.Location = new System.Drawing.Point(192, 42);
            this.panelRes0.Name = "panelRes0";
            this.panelRes0.Size = new System.Drawing.Size(55, 342);
            this.panelRes0.TabIndex = 28;
            this.panelRes0.Tag = "1";
            // 
            // labelEndet0
            // 
            this.labelEndet0.AutoSize = true;
            this.labelEndet0.Location = new System.Drawing.Point(3, 0);
            this.labelEndet0.Name = "labelEndet0";
            this.labelEndet0.Size = new System.Drawing.Size(41, 13);
            this.labelEndet0.TabIndex = 12;
            this.labelEndet0.Text = "label24";
            // 
            // labelSB0
            // 
            this.labelSB0.AutoSize = true;
            this.labelSB0.Location = new System.Drawing.Point(3, 22);
            this.labelSB0.Name = "labelSB0";
            this.labelSB0.Size = new System.Drawing.Size(41, 13);
            this.labelSB0.TabIndex = 11;
            this.labelSB0.Text = "label18";
            // 
            // labelPIB0
            // 
            this.labelPIB0.AutoSize = true;
            this.labelPIB0.Location = new System.Drawing.Point(3, 44);
            this.labelPIB0.Name = "labelPIB0";
            this.labelPIB0.Size = new System.Drawing.Size(35, 13);
            this.labelPIB0.TabIndex = 1;
            this.labelPIB0.Text = "label9";
            // 
            // labelRE0
            // 
            this.labelRE0.AutoSize = true;
            this.labelRE0.Location = new System.Drawing.Point(3, 75);
            this.labelRE0.Name = "labelRE0";
            this.labelRE0.Size = new System.Drawing.Size(41, 13);
            this.labelRE0.TabIndex = 8;
            this.labelRE0.Text = "label10";
            // 
            // labelDP0
            // 
            this.labelDP0.AutoSize = true;
            this.labelDP0.Location = new System.Drawing.Point(3, 97);
            this.labelDP0.Name = "labelDP0";
            this.labelDP0.Size = new System.Drawing.Size(41, 13);
            this.labelDP0.TabIndex = 10;
            this.labelDP0.Text = "label19";
            // 
            // labelPop0
            // 
            this.labelPop0.AutoSize = true;
            this.labelPop0.Location = new System.Drawing.Point(3, 129);
            this.labelPop0.Name = "labelPop0";
            this.labelPop0.Size = new System.Drawing.Size(35, 13);
            this.labelPop0.TabIndex = 2;
            this.labelPop0.Text = "label4";
            // 
            // labelTC0
            // 
            this.labelTC0.AutoSize = true;
            this.labelTC0.Location = new System.Drawing.Point(3, 167);
            this.labelTC0.Name = "labelTC0";
            this.labelTC0.Size = new System.Drawing.Size(35, 13);
            this.labelTC0.TabIndex = 3;
            this.labelTC0.Text = "label8";
            // 
            // labelRP0
            // 
            this.labelRP0.AutoSize = true;
            this.labelRP0.Location = new System.Drawing.Point(3, 192);
            this.labelRP0.Name = "labelRP0";
            this.labelRP0.Size = new System.Drawing.Size(41, 13);
            this.labelRP0.TabIndex = 9;
            this.labelRP0.Text = "label20";
            // 
            // labelImport0
            // 
            this.labelImport0.AutoSize = true;
            this.labelImport0.Location = new System.Drawing.Point(3, 231);
            this.labelImport0.Name = "labelImport0";
            this.labelImport0.Size = new System.Drawing.Size(35, 13);
            this.labelImport0.TabIndex = 4;
            this.labelImport0.Text = "label5";
            // 
            // labelExport0
            // 
            this.labelExport0.AutoSize = true;
            this.labelExport0.Location = new System.Drawing.Point(3, 253);
            this.labelExport0.Name = "labelExport0";
            this.labelExport0.Size = new System.Drawing.Size(41, 13);
            this.labelExport0.TabIndex = 5;
            this.labelExport0.Text = "label16";
            // 
            // labelConso0
            // 
            this.labelConso0.AutoSize = true;
            this.labelConso0.Location = new System.Drawing.Point(3, 291);
            this.labelConso0.Name = "labelConso0";
            this.labelConso0.Size = new System.Drawing.Size(41, 13);
            this.labelConso0.TabIndex = 6;
            this.labelConso0.Text = "label23";
            // 
            // labelRM0
            // 
            this.labelRM0.AutoSize = true;
            this.labelRM0.Location = new System.Drawing.Point(3, 321);
            this.labelRM0.Name = "labelRM0";
            this.labelRM0.Size = new System.Drawing.Size(41, 13);
            this.labelRM0.TabIndex = 7;
            this.labelRM0.Text = "label22";
            // 
            // labelObjectifs
            // 
            this.labelObjectifs.AutoSize = true;
            this.labelObjectifs.Location = new System.Drawing.Point(630, 18);
            this.labelObjectifs.Name = "labelObjectifs";
            this.labelObjectifs.Size = new System.Drawing.Size(48, 13);
            this.labelObjectifs.TabIndex = 27;
            this.labelObjectifs.Text = "Objectifs";
            // 
            // panelRes1
            // 
            this.panelRes1.Controls.Add(this.labelPIB1);
            this.panelRes1.Controls.Add(this.labelEndet1);
            this.panelRes1.Controls.Add(this.labelPop1);
            this.panelRes1.Controls.Add(this.labelSB1);
            this.panelRes1.Controls.Add(this.labelDP1);
            this.panelRes1.Controls.Add(this.labelRP1);
            this.panelRes1.Controls.Add(this.labelRE1);
            this.panelRes1.Controls.Add(this.labelRM1);
            this.panelRes1.Controls.Add(this.labelConso1);
            this.panelRes1.Controls.Add(this.labelTC1);
            this.panelRes1.Controls.Add(this.labelImport1);
            this.panelRes1.Controls.Add(this.labelExport1);
            this.panelRes1.Location = new System.Drawing.Point(295, 42);
            this.panelRes1.Name = "panelRes1";
            this.panelRes1.Size = new System.Drawing.Size(55, 342);
            this.panelRes1.TabIndex = 24;
            this.panelRes1.Tag = "1";
            this.panelRes1.Visible = false;
            // 
            // labelPIB1
            // 
            this.labelPIB1.AutoSize = true;
            this.labelPIB1.Location = new System.Drawing.Point(3, 0);
            this.labelPIB1.Name = "labelPIB1";
            this.labelPIB1.Size = new System.Drawing.Size(35, 13);
            this.labelPIB1.TabIndex = 8;
            this.labelPIB1.Text = "label9";
            // 
            // labelEndet1
            // 
            this.labelEndet1.AutoSize = true;
            this.labelEndet1.Location = new System.Drawing.Point(3, 22);
            this.labelEndet1.Name = "labelEndet1";
            this.labelEndet1.Size = new System.Drawing.Size(41, 13);
            this.labelEndet1.TabIndex = 23;
            this.labelEndet1.Text = "label24";
            // 
            // labelPop1
            // 
            this.labelPop1.AutoSize = true;
            this.labelPop1.Location = new System.Drawing.Point(3, 44);
            this.labelPop1.Name = "labelPop1";
            this.labelPop1.Size = new System.Drawing.Size(35, 13);
            this.labelPop1.TabIndex = 3;
            this.labelPop1.Text = "label4";
            // 
            // labelSB1
            // 
            this.labelSB1.AutoSize = true;
            this.labelSB1.Location = new System.Drawing.Point(3, 75);
            this.labelSB1.Name = "labelSB1";
            this.labelSB1.Size = new System.Drawing.Size(41, 13);
            this.labelSB1.TabIndex = 17;
            this.labelSB1.Text = "label18";
            // 
            // labelDP1
            // 
            this.labelDP1.AutoSize = true;
            this.labelDP1.Location = new System.Drawing.Point(3, 97);
            this.labelDP1.Name = "labelDP1";
            this.labelDP1.Size = new System.Drawing.Size(41, 13);
            this.labelDP1.TabIndex = 18;
            this.labelDP1.Text = "label19";
            // 
            // labelRP1
            // 
            this.labelRP1.AutoSize = true;
            this.labelRP1.Location = new System.Drawing.Point(3, 129);
            this.labelRP1.Name = "labelRP1";
            this.labelRP1.Size = new System.Drawing.Size(41, 13);
            this.labelRP1.TabIndex = 19;
            this.labelRP1.Text = "label20";
            // 
            // labelRE1
            // 
            this.labelRE1.AutoSize = true;
            this.labelRE1.Location = new System.Drawing.Point(3, 167);
            this.labelRE1.Name = "labelRE1";
            this.labelRE1.Size = new System.Drawing.Size(41, 13);
            this.labelRE1.TabIndex = 20;
            this.labelRE1.Text = "label21";
            // 
            // labelRM1
            // 
            this.labelRM1.AutoSize = true;
            this.labelRM1.Location = new System.Drawing.Point(3, 192);
            this.labelRM1.Name = "labelRM1";
            this.labelRM1.Size = new System.Drawing.Size(41, 13);
            this.labelRM1.TabIndex = 21;
            this.labelRM1.Text = "label22";
            // 
            // labelConso1
            // 
            this.labelConso1.AutoSize = true;
            this.labelConso1.Location = new System.Drawing.Point(3, 231);
            this.labelConso1.Name = "labelConso1";
            this.labelConso1.Size = new System.Drawing.Size(41, 13);
            this.labelConso1.TabIndex = 22;
            this.labelConso1.Text = "label23";
            // 
            // labelTC1
            // 
            this.labelTC1.AutoSize = true;
            this.labelTC1.Location = new System.Drawing.Point(3, 253);
            this.labelTC1.Name = "labelTC1";
            this.labelTC1.Size = new System.Drawing.Size(35, 13);
            this.labelTC1.TabIndex = 7;
            this.labelTC1.Text = "label8";
            // 
            // labelImport1
            // 
            this.labelImport1.AutoSize = true;
            this.labelImport1.Location = new System.Drawing.Point(3, 291);
            this.labelImport1.Name = "labelImport1";
            this.labelImport1.Size = new System.Drawing.Size(35, 13);
            this.labelImport1.TabIndex = 4;
            this.labelImport1.Text = "label5";
            // 
            // labelExport1
            // 
            this.labelExport1.AutoSize = true;
            this.labelExport1.Location = new System.Drawing.Point(3, 321);
            this.labelExport1.Name = "labelExport1";
            this.labelExport1.Size = new System.Drawing.Size(41, 13);
            this.labelExport1.TabIndex = 15;
            this.labelExport1.Text = "label16";
            // 
            // labelDepensesPubliques
            // 
            this.labelDepensesPubliques.AutoSize = true;
            this.labelDepensesPubliques.Location = new System.Drawing.Point(47, 295);
            this.labelDepensesPubliques.Name = "labelDepensesPubliques";
            this.labelDepensesPubliques.Size = new System.Drawing.Size(113, 13);
            this.labelDepensesPubliques.TabIndex = 16;
            this.labelDepensesPubliques.Text = "Dépenses budgétaires";
            // 
            // labelRevenusEntreprises
            // 
            this.labelRevenusEntreprises.AutoSize = true;
            this.labelRevenusEntreprises.Location = new System.Drawing.Point(47, 234);
            this.labelRevenusEntreprises.Name = "labelRevenusEntreprises";
            this.labelRevenusEntreprises.Size = new System.Drawing.Size(119, 13);
            this.labelRevenusEntreprises.TabIndex = 14;
            this.labelRevenusEntreprises.Text = "Revenu des entreprises";
            // 
            // labelImport
            // 
            this.labelImport.AutoSize = true;
            this.labelImport.Location = new System.Drawing.Point(47, 117);
            this.labelImport.Name = "labelImport";
            this.labelImport.Size = new System.Drawing.Size(64, 13);
            this.labelImport.TabIndex = 13;
            this.labelImport.Text = "Importations";
            // 
            // labelRevenusMenages
            // 
            this.labelRevenusMenages.AutoSize = true;
            this.labelRevenusMenages.Location = new System.Drawing.Point(47, 209);
            this.labelRevenusMenages.Name = "labelRevenusMenages";
            this.labelRevenusMenages.Size = new System.Drawing.Size(111, 13);
            this.labelRevenusMenages.TabIndex = 12;
            this.labelRevenusMenages.Text = "Revenu des ménages";
            // 
            // labelPopActive
            // 
            this.labelPopActive.AutoSize = true;
            this.labelPopActive.Location = new System.Drawing.Point(47, 64);
            this.labelPopActive.Name = "labelPopActive";
            this.labelPopActive.Size = new System.Drawing.Size(143, 13);
            this.labelPopActive.TabIndex = 11;
            this.labelPopActive.Text = "Population active (en milliers)";
            // 
            // labelExport
            // 
            this.labelExport.AutoSize = true;
            this.labelExport.Location = new System.Drawing.Point(47, 139);
            this.labelExport.Name = "labelExport";
            this.labelExport.Size = new System.Drawing.Size(65, 13);
            this.labelExport.TabIndex = 10;
            this.labelExport.Text = "Exportations";
            // 
            // labelRecettesPubliques
            // 
            this.labelRecettesPubliques.AutoSize = true;
            this.labelRecettesPubliques.Location = new System.Drawing.Point(47, 273);
            this.labelRecettesPubliques.Name = "labelRecettesPubliques";
            this.labelRecettesPubliques.Size = new System.Drawing.Size(108, 13);
            this.labelRecettesPubliques.TabIndex = 9;
            this.labelRecettesPubliques.Text = "Recettes budgétaires";
            // 
            // labelSoldeBudgetaire
            // 
            this.labelSoldeBudgetaire.AutoSize = true;
            this.labelSoldeBudgetaire.Location = new System.Drawing.Point(47, 333);
            this.labelSoldeBudgetaire.Name = "labelSoldeBudgetaire";
            this.labelSoldeBudgetaire.Size = new System.Drawing.Size(87, 13);
            this.labelSoldeBudgetaire.TabIndex = 6;
            this.labelSoldeBudgetaire.Text = "Solde budgetaire";
            // 
            // labelEndettement
            // 
            this.labelEndettement.AutoSize = true;
            this.labelEndettement.Location = new System.Drawing.Point(47, 363);
            this.labelEndettement.Name = "labelEndettement";
            this.labelEndettement.Size = new System.Drawing.Size(67, 13);
            this.labelEndettement.TabIndex = 5;
            this.labelEndettement.Text = "Endettement";
            // 
            // labelConso
            // 
            this.labelConso.AutoSize = true;
            this.labelConso.Location = new System.Drawing.Point(47, 171);
            this.labelConso.Name = "labelConso";
            this.labelConso.Size = new System.Drawing.Size(76, 13);
            this.labelConso.TabIndex = 2;
            this.labelConso.Text = "Consommation";
            // 
            // labelTauxChomage
            // 
            this.labelTauxChomage.AutoSize = true;
            this.labelTauxChomage.Location = new System.Drawing.Point(47, 86);
            this.labelTauxChomage.Name = "labelTauxChomage";
            this.labelTauxChomage.Size = new System.Drawing.Size(93, 13);
            this.labelTauxChomage.TabIndex = 1;
            this.labelTauxChomage.Text = "Taux de chômage";
            // 
            // labelPIB
            // 
            this.labelPIB.AutoSize = true;
            this.labelPIB.Location = new System.Drawing.Point(47, 42);
            this.labelPIB.Name = "labelPIB";
            this.labelPIB.Size = new System.Drawing.Size(24, 13);
            this.labelPIB.TabIndex = 0;
            this.labelPIB.Text = "PIB";
            // 
            // labelDesDP
            // 
            this.labelDesDP.AutoSize = true;
            this.labelDesDP.Location = new System.Drawing.Point(179, 63);
            this.labelDesDP.Name = "labelDesDP";
            this.labelDesDP.Size = new System.Drawing.Size(25, 13);
            this.labelDesDP.TabIndex = 16;
            this.labelDesDP.Text = "400";
            // 
            // labelDesInvestissements
            // 
            this.labelDesInvestissements.AutoSize = true;
            this.labelDesInvestissements.Location = new System.Drawing.Point(179, 99);
            this.labelDesInvestissements.Name = "labelDesInvestissements";
            this.labelDesInvestissements.Size = new System.Drawing.Size(25, 13);
            this.labelDesInvestissements.TabIndex = 15;
            this.labelDesInvestissements.Text = "400";
            // 
            // labelDesAides
            // 
            this.labelDesAides.AutoSize = true;
            this.labelDesAides.Location = new System.Drawing.Point(179, 131);
            this.labelDesAides.Name = "labelDesAides";
            this.labelDesAides.Size = new System.Drawing.Size(25, 13);
            this.labelDesAides.TabIndex = 14;
            this.labelDesAides.Text = "400";
            // 
            // buttonAidesMoins
            // 
            this.buttonAidesMoins.Location = new System.Drawing.Point(270, 126);
            this.buttonAidesMoins.Name = "buttonAidesMoins";
            this.buttonAidesMoins.Size = new System.Drawing.Size(29, 23);
            this.buttonAidesMoins.TabIndex = 13;
            this.buttonAidesMoins.Text = "-";
            this.toolTipDecisions.SetToolTip(this.buttonAidesMoins, "... et inversement");
            this.buttonAidesMoins.UseVisualStyleBackColor = true;
            this.buttonAidesMoins.Click += new System.EventHandler(this.buttonAidesMoins_Click);
            // 
            // buttonAidesPlus
            // 
            this.buttonAidesPlus.Location = new System.Drawing.Point(235, 126);
            this.buttonAidesPlus.Name = "buttonAidesPlus";
            this.buttonAidesPlus.Size = new System.Drawing.Size(29, 23);
            this.buttonAidesPlus.TabIndex = 12;
            this.buttonAidesPlus.Text = "+";
            this.toolTipDecisions.SetToolTip(this.buttonAidesPlus, "Augmenter les aides a pour effet d\'augmenter les dépenses de l\'état\r\nMais aussi d" +
        "\'augmenter la consommation\r\n");
            this.buttonAidesPlus.UseVisualStyleBackColor = true;
            this.buttonAidesPlus.Click += new System.EventHandler(this.buttonAidesPlus_Click);
            // 
            // buttonInvMoins
            // 
            this.buttonInvMoins.Location = new System.Drawing.Point(270, 94);
            this.buttonInvMoins.Name = "buttonInvMoins";
            this.buttonInvMoins.Size = new System.Drawing.Size(29, 23);
            this.buttonInvMoins.TabIndex = 11;
            this.buttonInvMoins.Text = "-";
            this.toolTipDecisions.SetToolTip(this.buttonInvMoins, "... et inversement");
            this.buttonInvMoins.UseVisualStyleBackColor = true;
            this.buttonInvMoins.Click += new System.EventHandler(this.buttonInvMoins_Click);
            // 
            // buttonInvPlus
            // 
            this.buttonInvPlus.Location = new System.Drawing.Point(235, 94);
            this.buttonInvPlus.Name = "buttonInvPlus";
            this.buttonInvPlus.Size = new System.Drawing.Size(29, 23);
            this.buttonInvPlus.TabIndex = 10;
            this.buttonInvPlus.Text = "+";
            this.toolTipDecisions.SetToolTip(this.buttonInvPlus, "Augmenter les investissements a pour effet d\'augmenter les dépenses de l\'état\r\nMa" +
        "is aussi de diminuer le taux de chômage, d\'augmenter les importations et la cons" +
        "ommation\r\n");
            this.buttonInvPlus.UseVisualStyleBackColor = true;
            this.buttonInvPlus.Click += new System.EventHandler(this.buttonInvPlus_Click);
            // 
            // buttonDPMoins
            // 
            this.buttonDPMoins.Location = new System.Drawing.Point(270, 58);
            this.buttonDPMoins.Name = "buttonDPMoins";
            this.buttonDPMoins.Size = new System.Drawing.Size(29, 23);
            this.buttonDPMoins.TabIndex = 9;
            this.buttonDPMoins.Text = "-";
            this.toolTipDecisions.SetToolTip(this.buttonDPMoins, "... et inversement");
            this.buttonDPMoins.UseVisualStyleBackColor = true;
            this.buttonDPMoins.Click += new System.EventHandler(this.buttonDPMoins_Click);
            // 
            // buttonDPPlus
            // 
            this.buttonDPPlus.Location = new System.Drawing.Point(235, 58);
            this.buttonDPPlus.Name = "buttonDPPlus";
            this.buttonDPPlus.Size = new System.Drawing.Size(29, 23);
            this.buttonDPPlus.TabIndex = 8;
            this.buttonDPPlus.Text = "+";
            this.toolTipDecisions.SetToolTip(this.buttonDPPlus, "Augmenter les dépenses diverses a pour effet d\'augmenter les dépenses de l\'état\r\n" +
        "Mais aussi d\'augmenter la consommation");
            this.buttonDPPlus.UseVisualStyleBackColor = true;
            this.buttonDPPlus.Click += new System.EventHandler(this.buttonDPPlus_Click);
            // 
            // buttonImpotsMoins
            // 
            this.buttonImpotsMoins.Location = new System.Drawing.Point(270, 28);
            this.buttonImpotsMoins.Name = "buttonImpotsMoins";
            this.buttonImpotsMoins.Size = new System.Drawing.Size(29, 23);
            this.buttonImpotsMoins.TabIndex = 7;
            this.buttonImpotsMoins.Text = "-";
            this.toolTipDecisions.SetToolTip(this.buttonImpotsMoins, "... et inversement");
            this.buttonImpotsMoins.UseVisualStyleBackColor = true;
            this.buttonImpotsMoins.Click += new System.EventHandler(this.buttonImpotsMoins_Click);
            // 
            // buttonImpotsPlus
            // 
            this.buttonImpotsPlus.Location = new System.Drawing.Point(235, 28);
            this.buttonImpotsPlus.Name = "buttonImpotsPlus";
            this.buttonImpotsPlus.Size = new System.Drawing.Size(29, 23);
            this.buttonImpotsPlus.TabIndex = 6;
            this.buttonImpotsPlus.Text = "+";
            this.toolTipDecisions.SetToolTip(this.buttonImpotsPlus, "Augmenter les impôts a pour effet d\'augmenter les recettes publiques\r\nMais aussi " +
        "d\'augmenter le taux de chômage et de réduire la consommation");
            this.buttonImpotsPlus.UseVisualStyleBackColor = true;
            this.buttonImpotsPlus.Click += new System.EventHandler(this.buttonImpotsPlus_Click);
            // 
            // labelDesImpots
            // 
            this.labelDesImpots.AutoSize = true;
            this.labelDesImpots.Location = new System.Drawing.Point(179, 33);
            this.labelDesImpots.Name = "labelDesImpots";
            this.labelDesImpots.Size = new System.Drawing.Size(27, 13);
            this.labelDesImpots.TabIndex = 5;
            this.labelDesImpots.Text = "20%";
            // 
            // labelAides
            // 
            this.labelAides.AutoSize = true;
            this.labelAides.Location = new System.Drawing.Point(20, 131);
            this.labelAides.Name = "labelAides";
            this.labelAides.Size = new System.Drawing.Size(33, 13);
            this.labelAides.TabIndex = 4;
            this.labelAides.Text = "Aides";
            this.toolTipDecisions.SetToolTip(this.labelAides, "Cela comprend les subventions aux entreprises et les prestations sociales");
            // 
            // labelInvestissements
            // 
            this.labelInvestissements.AutoSize = true;
            this.labelInvestissements.Location = new System.Drawing.Point(20, 99);
            this.labelInvestissements.Name = "labelInvestissements";
            this.labelInvestissements.Size = new System.Drawing.Size(82, 13);
            this.labelInvestissements.TabIndex = 3;
            this.labelInvestissements.Text = "Investissements";
            this.toolTipDecisions.SetToolTip(this.labelInvestissements, "Cela comprend l\'emploi public et les investissements publics");
            // 
            // labelDepenses
            // 
            this.labelDepenses.AutoSize = true;
            this.labelDepenses.Location = new System.Drawing.Point(20, 63);
            this.labelDepenses.Name = "labelDepenses";
            this.labelDepenses.Size = new System.Drawing.Size(97, 13);
            this.labelDepenses.TabIndex = 2;
            this.labelDepenses.Text = "Dépenses diverses";
            this.toolTipDecisions.SetToolTip(this.labelDepenses, "Cela comprend le remboursement de la sécu, les travaux publics et les allocations" +
        " chômages");
            // 
            // labelImpots
            // 
            this.labelImpots.AutoSize = true;
            this.labelImpots.Location = new System.Drawing.Point(20, 33);
            this.labelImpots.Name = "labelImpots";
            this.labelImpots.Size = new System.Drawing.Size(38, 13);
            this.labelImpots.TabIndex = 1;
            this.labelImpots.Text = "Impôts";
            this.toolTipDecisions.SetToolTip(this.labelImpots, "Taux d\'imposition sur le revenu des ménages, sur les bénéfices des entreprises et" +
        " la TVA");
            // 
            // toolTipDecisions
            // 
            this.toolTipDecisions.AutoPopDelay = 5000000;
            this.toolTipDecisions.InitialDelay = 500;
            this.toolTipDecisions.IsBalloon = true;
            this.toolTipDecisions.ReshowDelay = 100;
            this.toolTipDecisions.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(851, 213);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(350, 205);
            this.tabControl1.TabIndex = 17;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.bSuivant);
            this.tabPage1.Controls.Add(this.labelImpots);
            this.tabPage1.Controls.Add(this.labelDesDP);
            this.tabPage1.Controls.Add(this.buttonDPPlus);
            this.tabPage1.Controls.Add(this.buttonImpotsMoins);
            this.tabPage1.Controls.Add(this.labelDesInvestissements);
            this.tabPage1.Controls.Add(this.buttonDPMoins);
            this.tabPage1.Controls.Add(this.buttonImpotsPlus);
            this.tabPage1.Controls.Add(this.labelDesAides);
            this.tabPage1.Controls.Add(this.buttonInvPlus);
            this.tabPage1.Controls.Add(this.labelDepenses);
            this.tabPage1.Controls.Add(this.labelDesImpots);
            this.tabPage1.Controls.Add(this.buttonAidesMoins);
            this.tabPage1.Controls.Add(this.buttonInvMoins);
            this.tabPage1.Controls.Add(this.labelInvestissements);
            this.tabPage1.Controls.Add(this.labelAides);
            this.tabPage1.Controls.Add(this.buttonAidesPlus);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(342, 179);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Décisions";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // bSuivant
            // 
            this.bSuivant.Location = new System.Drawing.Point(306, 153);
            this.bSuivant.Name = "bSuivant";
            this.bSuivant.Size = new System.Drawing.Size(30, 23);
            this.bSuivant.TabIndex = 17;
            this.bSuivant.Text = ">";
            this.bSuivant.UseVisualStyleBackColor = true;
            this.bSuivant.Click += new System.EventHandler(this.bSuivant_Click);
            // 
            // chartPIB
            // 
            chartArea1.Name = "ChartArea1";
            this.chartPIB.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartPIB.Legends.Add(legend1);
            this.chartPIB.Location = new System.Drawing.Point(30, 456);
            this.chartPIB.Name = "chartPIB";
            series1.BorderWidth = 3;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "PIB";
            series2.BorderWidth = 3;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.Red;
            series2.LabelBorderColor = System.Drawing.Color.White;
            series2.Legend = "Legend1";
            series2.Name = "Objectif";
            series2.Points.Add(dataPoint1);
            series2.Points.Add(dataPoint2);
            series2.Points.Add(dataPoint3);
            series2.Points.Add(dataPoint4);
            this.chartPIB.Series.Add(series1);
            this.chartPIB.Series.Add(series2);
            this.chartPIB.Size = new System.Drawing.Size(362, 216);
            this.chartPIB.TabIndex = 18;
            this.chartPIB.Text = "chart1";
            // 
            // chartTC
            // 
            chartArea2.Name = "ChartArea1";
            this.chartTC.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartTC.Legends.Add(legend2);
            this.chartTC.Location = new System.Drawing.Point(398, 456);
            this.chartTC.Name = "chartTC";
            series3.BorderWidth = 3;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "Taux de chomage";
            series4.BorderWidth = 3;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Color = System.Drawing.Color.Red;
            series4.LabelBorderColor = System.Drawing.Color.White;
            series4.Legend = "Legend1";
            series4.Name = "Objectif";
            series4.Points.Add(dataPoint5);
            series4.Points.Add(dataPoint6);
            series4.Points.Add(dataPoint7);
            series4.Points.Add(dataPoint8);
            this.chartTC.Series.Add(series3);
            this.chartTC.Series.Add(series4);
            this.chartTC.Size = new System.Drawing.Size(380, 216);
            this.chartTC.TabIndex = 19;
            this.chartTC.Text = "chart1";
            // 
            // chartSB
            // 
            chartArea3.Name = "ChartArea1";
            this.chartSB.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chartSB.Legends.Add(legend3);
            this.chartSB.Location = new System.Drawing.Point(790, 456);
            this.chartSB.Name = "chartSB";
            series5.BorderWidth = 3;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Legend = "Legend1";
            series5.Name = "Solde budgétaire";
            series6.BorderWidth = 3;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Color = System.Drawing.Color.Red;
            series6.LabelBorderColor = System.Drawing.Color.White;
            series6.Legend = "Legend1";
            series6.Name = "Objectif";
            series6.Points.Add(dataPoint9);
            series6.Points.Add(dataPoint10);
            series6.Points.Add(dataPoint11);
            series6.Points.Add(dataPoint12);
            this.chartSB.Series.Add(series5);
            this.chartSB.Series.Add(series6);
            this.chartSB.Size = new System.Drawing.Size(365, 216);
            this.chartSB.TabIndex = 20;
            this.chartSB.Text = "chart1";
            // 
            // tcInstructions
            // 
            this.tcInstructions.Controls.Add(this.Instructions);
            this.tcInstructions.Location = new System.Drawing.Point(851, 34);
            this.tcInstructions.Name = "tcInstructions";
            this.tcInstructions.SelectedIndex = 0;
            this.tcInstructions.Size = new System.Drawing.Size(350, 152);
            this.tcInstructions.TabIndex = 21;
            // 
            // Instructions
            // 
            this.Instructions.Controls.Add(this.labelInstructions);
            this.Instructions.Location = new System.Drawing.Point(4, 22);
            this.Instructions.Name = "Instructions";
            this.Instructions.Padding = new System.Windows.Forms.Padding(3);
            this.Instructions.Size = new System.Drawing.Size(342, 126);
            this.Instructions.TabIndex = 0;
            this.Instructions.Text = "Instructions";
            this.Instructions.UseVisualStyleBackColor = true;
            // 
            // labelInstructions
            // 
            this.labelInstructions.AutoSize = true;
            this.labelInstructions.Location = new System.Drawing.Point(6, 20);
            this.labelInstructions.Name = "labelInstructions";
            this.labelInstructions.Size = new System.Drawing.Size(319, 52);
            this.labelInstructions.TabIndex = 0;
            this.labelInstructions.Text = resources.GetString("labelInstructions.Text");
            // 
            // FormJeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1274, 684);
            this.Controls.Add(this.tcInstructions);
            this.Controls.Add(this.chartSB);
            this.Controls.Add(this.chartTC);
            this.Controls.Add(this.chartPIB);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.tcJeu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormJeu";
            this.Text = "#OPA";
            this.tcJeu.ResumeLayout(false);
            this.tpResultats.ResumeLayout(false);
            this.tpResultats.PerformLayout();
            this.panelRes3.ResumeLayout(false);
            this.panelRes3.PerformLayout();
            this.panelRes2.ResumeLayout(false);
            this.panelRes2.PerformLayout();
            this.panelRes0.ResumeLayout(false);
            this.panelRes0.PerformLayout();
            this.panelRes1.ResumeLayout(false);
            this.panelRes1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPIB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartTC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartSB)).EndInit();
            this.tcInstructions.ResumeLayout(false);
            this.Instructions.ResumeLayout(false);
            this.Instructions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcJeu;
        private System.Windows.Forms.TabPage tpResultats;
        private System.Windows.Forms.Label labelPIB;
        private System.Windows.Forms.Label labelDepensesPubliques;
        private System.Windows.Forms.Label labelExport1;
        private System.Windows.Forms.Label labelRevenusEntreprises;
        private System.Windows.Forms.Label labelImport;
        private System.Windows.Forms.Label labelRevenusMenages;
        private System.Windows.Forms.Label labelPopActive;
        private System.Windows.Forms.Label labelExport;
        private System.Windows.Forms.Label labelRecettesPubliques;
        private System.Windows.Forms.Label labelPIB1;
        private System.Windows.Forms.Label labelTC1;
        private System.Windows.Forms.Label labelSoldeBudgetaire;
        private System.Windows.Forms.Label labelEndettement;
        private System.Windows.Forms.Label labelImport1;
        private System.Windows.Forms.Label labelPop1;
        private System.Windows.Forms.Label labelConso;
        private System.Windows.Forms.Label labelTauxChomage;
        private System.Windows.Forms.Panel panelRes1;
        private System.Windows.Forms.Label labelEndet1;
        private System.Windows.Forms.Label labelSB1;
        private System.Windows.Forms.Label labelDP1;
        private System.Windows.Forms.Label labelRP1;
        private System.Windows.Forms.Label labelRE1;
        private System.Windows.Forms.Label labelRM1;
        private System.Windows.Forms.Label labelConso1;
        private System.Windows.Forms.Label labelObjectifs;
        private System.Windows.Forms.Label labelDesImpots;
        private System.Windows.Forms.Label labelAides;
        private System.Windows.Forms.Label labelInvestissements;
        private System.Windows.Forms.Label labelDepenses;
        private System.Windows.Forms.Label labelImpots;
        private System.Windows.Forms.Button buttonImpotsMoins;
        private System.Windows.Forms.Button buttonImpotsPlus;
        private System.Windows.Forms.Button buttonAidesMoins;
        private System.Windows.Forms.Button buttonAidesPlus;
        private System.Windows.Forms.Button buttonInvMoins;
        private System.Windows.Forms.Button buttonInvPlus;
        private System.Windows.Forms.Button buttonDPMoins;
        private System.Windows.Forms.Button buttonDPPlus;
        private System.Windows.Forms.Label labelDesDP;
        private System.Windows.Forms.Label labelDesInvestissements;
        private System.Windows.Forms.Label labelDesAides;
        private System.Windows.Forms.ToolTip toolTipDecisions;
        private System.Windows.Forms.Panel panelRes0;
        private System.Windows.Forms.Label labelPIB0;
        private System.Windows.Forms.Label labelEndet0;
        private System.Windows.Forms.Label labelPop0;
        private System.Windows.Forms.Label labelSB0;
        private System.Windows.Forms.Label labelDP0;
        private System.Windows.Forms.Label labelRP0;
        private System.Windows.Forms.Label labelRM0;
        private System.Windows.Forms.Label labelConso0;
        private System.Windows.Forms.Label labelTC0;
        private System.Windows.Forms.Label labelImport0;
        private System.Windows.Forms.Label labelExport0;
        private System.Windows.Forms.Label labelRE0;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button buttonRecommencer;
        private System.Windows.Forms.Panel panelRes3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panelRes2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button bSuivant;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPIB;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTC;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSB;
        private System.Windows.Forms.TabControl tcInstructions;
        private System.Windows.Forms.TabPage Instructions;
        private System.Windows.Forms.Label labelInstructions;
    }
}

