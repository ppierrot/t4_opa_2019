﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hashtag_OPA
{
    public partial class FormJeu : Form
    {
        private int tour;
        private Pays p;
        public FormJeu()
        {
            InitializeComponent();
            initialiserJeu();
        }

        //initialise le jeu
        private void initialiserJeu()
        {
            //
            //initialiser les paramètres du jeu
            tour = 0;
            p = G_Jeu.jeuDessai();

            afficherResultatsTour(panelRes0);

            labelDesImpots.Text = "15%";
            labelDesDP.Text = "600";
            labelDesInvestissements.Text = "600";
            labelDesAides.Text = "600";

            //définir les objectifs à atteindre
        }

        //fini le jeu
        private void finDuJeu()
        {
            buttonRecommencer.Visible = true;
            bSuivant.Visible = false;

            tcJeu.SelectedIndex = 0;

            //vérifier si les objectifs ont été atteints
            if (p.PIB >= 12000 && p.tauxChomage <= 2 && p.soldeBudgetaire > 0)
            {
                labelInstructions.Text = "Félicitations ! Vous avez atteint les objectifs";
            }
            else
            {
                labelInstructions.Text = "Défaite ! Vous n'avez pas atteint les objectifs";
            }

        }

        //recommence le jeu
        private void recommencer()
        {
            panelRes1.Visible = false;
            panelRes2.Visible = false;
            panelRes3.Visible = false;
            initialiserJeu();

            buttonRecommencer.Visible = false;
            bSuivant.Visible = true;

            chartPIB.Series["PIB"].Points.Clear();
            chartTC.Series["Taux de chomage"].Points.Clear();
            chartSB.Series["Solde budgétaire"].Points.Clear();
            afficherResultatsTour(panelRes0);

            labelInstructions.Text = "Vous êtes à la tête d'un pays et vous devez prendre des décisions\n" +
                                        "pour réaliser les objectifs pré-établis. Gardez en tête que cela reste\n" +
                                        "une simulation et ne représente qu'une infime partie de ce qu'il\n" +
                                        "se passerait en réalité";
        }

        //passe au tour suivant
        private void bSuivant_Click(object sender, EventArgs e)
        {
            //
            //passer au tour suivant
            tour++;

            if (tour == 4)
            {
                recommencer();
                return;
            }

            //
            //récupérer les décisions
            //
            int impots = int.Parse(labelDesImpots.Text.Substring(0, labelDesImpots.Text.Length - 1));
            int depenses = int.Parse(labelDesDP.Text);
            int investissements = int.Parse(labelDesInvestissements.Text);
            int aides = int.Parse(labelDesAides.Text);

            int[] decisions = new int[4];
            decisions[0] = impots;
            decisions[1] = depenses;
            decisions[2] = investissements;
            decisions[3] = aides;

            G_Jeu.tourSuivant(p, decisions);

            //
            //panel à utiliser selon le tour
            switch (tour)
            {
                case 1:
                    panelRes1.Visible = true;
                    afficherResultatsTour(panelRes1);
                    break;
                case 2:
                    panelRes2.Visible = true;
                    afficherResultatsTour(panelRes2);
                    break;
                case 3:
                    panelRes3.Visible = true;
                    afficherResultatsTour(panelRes3);
                    finDuJeu();
                    break;
            }

            //
            //mettre le focus sur les décisions
            tcJeu.SelectedIndex = 0;
        }

        private void buttonImpotsPlus_Click(object sender, EventArgs e)
        {
            int i = int.Parse(labelDesImpots.Text.Substring(0, labelDesImpots.Text.Length - 1));
            i++;
            if (i <= 40)
            {
                labelDesImpots.Text = i + "%";
            }
            else
            {
            }
        }

        private void buttonImpotsMoins_Click(object sender, EventArgs e)
        {
            int i = int.Parse(labelDesImpots.Text.Substring(0, labelDesImpots.Text.Length - 1));
            i--;
            if (i >= 0)
            {
                labelDesImpots.Text = i + "%";
            }
            else
            {
                MessageBox.Show("Vous voulez donner de l'argent ou prélever des impôts ?", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonDPPlus_Click(object sender, EventArgs e)
        {
            int i = int.Parse(labelDesDP.Text);
            i += 100;
            if (i <= 1000)
            {
                labelDesDP.Text = i + "";
            }
            else
            {
            }
        }

        private void buttonDPMoins_Click(object sender, EventArgs e)
        {
            int i = int.Parse(labelDesDP.Text);
            i -= 100;
            if (i >= 0)
            {
                labelDesDP.Text = i + "";
            }
            else
            {
            }
        }

        private void buttonInvPlus_Click(object sender, EventArgs e)
        {
            int i = int.Parse(labelDesInvestissements.Text);
            i += 100;
            if (i <= 1000)
            {
                labelDesInvestissements.Text = i + "";
            }
            else
            {
            }
        }

        private void buttonInvMoins_Click(object sender, EventArgs e)
        {
            int i = int.Parse(labelDesInvestissements.Text);
            i -= 100;
            if (i >= 0)
            {
                labelDesInvestissements.Text = i + "";
            }
            else
            {
            }
        }

        private void buttonAidesPlus_Click(object sender, EventArgs e)
        {
            int i = int.Parse(labelDesAides.Text);
            i += 100;
            if (i <= 1000)
            {
                labelDesAides.Text = i + "";
            }
            else
            {
            }
        }

        private void buttonAidesMoins_Click(object sender, EventArgs e)
        {
            int i = int.Parse(labelDesAides.Text);
            i -= 100;
            if (i >= 0)
            {
                labelDesAides.Text = i + "";
            }
            else
            {
            }
        }

        //affiche les résultats du pays dans le panel en paramètre
        private void afficherResultatsTour(Panel pa)
        {
            double[] valeursPays = G_Jeu.valeursPays(p);


            chartPIB.Series["PIB"].Points.AddXY(0, valeursPays[0]);
            chartTC.Series["Taux de chomage"].Points.AddXY(0, valeursPays[2]);
            chartSB.Series["Solde budgétaire"].Points.AddXY(0, valeursPays[10]);

            int i = 0;
            foreach (Label l in pa.Controls)
            {
                l.Text = valeursPays[i] + "";
                i++;
            }

        }

        private void buttonRecommencer_Click(object sender, EventArgs e)
        {
            recommencer();
        }
    }
}
