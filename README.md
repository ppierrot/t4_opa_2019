\# OpérationPrésidenceAnticonjoncturelle
=============================================
![Logo](./images/OPA_logo.png)

Projet T4 : Incarner le président d'un pays dans le contexte de la gestion de l'économie du dît pays
----------------------------------------------------------------------------------------------------

### Liens du projet : 

[Lien vers le google doc/cahier des charges](https://docs.google.com/document/d/1p9t3c3eFpf6rRYw0Sd4fOW0rPRbqikDWMZs1Y6Fn2BY/edit#)

[Lien de téléchargement de l'exécutable](https://git.unistra.fr/ppastore/mac19-t4-e/raw/master/hashtag-OPA.exe?inline=false)

## Présentation du projet :

Dans le cadre d'un projet tutoré, nous avons eu pour tâche de réaliser un cahier des charges destiné à des étudiants de DUT Informatique de 2ème année.

Dans #OpérationPrésidenceAnticonjoncturelle, un serious game, le joueur incarnant le président d’un pays devra gérer les différents paramètres qui définissent un pays afin d’atteindre un objectif économique qui lui a été donné. Ainsi, en prenant des décisions, le président devra, par exemple, modifier le taux de TVA ou le taux d'imposition sur les bénéfices des entreprises pour augmenter la recette publique. 

### MVC du prototype :

![MVCproto](./images/MVCpro.png)


### Pôles de gestion utilisé dans le prototype :

- PIB
- Population active
- Taux de chômage
- Importations
- Exportations
- Consommation
- Revenu des ménages
- Revenu des entreprises
- Recettes publiques
- Dépenses publiques
- Solde budgétaire
- Endettement

### Influence des décisions dans les dîts pôles :

- Impôts
- Dépenses publiques
- Investissements
- Aides

## Développeurs

  - Abid Othmane
  - Pierre Pastore
  - Onésime Attica
  - Yasin Aydogdu

Plus d'explications sur le projet sont disponibles dans le cahier des charges.
  
